<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//Route Tabel Lumen
$router->post('/tambahdata', 'PostController@inputdata');
$router->get('/views', 'PostController@index');
$router->get('/view/{id}', 'PostController@view');
$router->put('/update/{id}', 'PostController@update');
$router->post('/update/{id}', 'PostController@updatedata');
$router->delete('/delete/{id}', 'PostController@delete');
// Route Untuk Tabel Data Kurban
$router->post('/tambahdatakurban', 'C_DataKurban@inputdata');

